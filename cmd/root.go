/*
* SPDX-License-Identifier: GPL-3.0-only
* Copyright (C) 2024 Kevin Suñer <ksuner@pm.me>
 */

package cmd

import (
	"flag"
	"log"
	"os"
)

type Commander map[string]Command

type Command struct {
    Flags   *flag.FlagSet
    Env     map[string]*string
    Info    string
    Usage   string
    Run     func(cmd Command)
}

var logger = log.New(os.Stderr, "[ERROR] ", 0) // the space is on purpose
var commander = make(Commander, 0)

func Start() Commander {
    commander["root"] = Command{
        Flags: flag.NewFlagSet("root", flag.ExitOnError),
        Env: nil,
        Info: `A minimalist static web page generator that lives in your terminal,
built for individuals that prioritize content over features.

Copyright (C) 2024 Kevin Suñer <ksuner@pm.me>
This program is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software Foundation, 
version 3.`,
        Usage: `Usage:
    statico [command] [flags]
    statico init -help
    statico generate -help`,
        Run: func(cmd Command) {},
    }

    return commander
}

