package cmd

import (
	"html/template"
	"strings"
	"testing"
)

func Test_parseMarkdown(t *testing.T) {
    tests := []struct{
        name            string
        dir             string
        contents        map[string]content
        postsMetadata   []map[string]interface{}
        wantErr         bool
    }{
        {
            name: "happy path",
            dir: "../testdata/test_parse_markdown_good",
            contents: map[string]content{
                "test": {
                    kind: "content",
                    meta: map[string]interface{}{
                        "title": "test",
                    },
                    html: template.HTML("<p>test.md content</p>"),
                },
                "test_post": {
                    kind: "post",
                    meta: map[string]interface{}{
                        "title": "test_post",
                    },
                    html: template.HTML("<p>test_post.md content</p>"),
                },
            },
            postsMetadata: []map[string]interface{}{
                {"title": "test_post"},
            },
            wantErr: false,
        },
        {
            name: "invalid yaml in metadata",
            dir: "../testdata/test_parse_markdown_bad",
            contents: nil,
            postsMetadata: nil,
            wantErr: true,
        },
    }

    for _, tc := range tests {
        t.Run(tc.name, func(t *testing.T) {
            contents, postsMetadata, err := parseMarkdown(tc.dir)
            if err != nil && tc.wantErr == false {
                t.Fatalf("unexpected error, got=%v", err)
            }

            if tc.contents != nil && tc.postsMetadata != nil {
                for key, val := range contents {
                    if val.kind != tc.contents[key].kind {
                        t.Fatalf("unexpected kind, got=%s, want=%s", 
                            val.kind, tc.contents[key].kind)
                    }

                    if val.meta["title"] != tc.contents[key].meta["title"] {
                        t.Fatalf("unexpected meta, got=%v, want=%v", 
                            val.meta["title"], tc.contents[key].meta["title"])
                    }

                    got := strings.TrimRight(string(val.html), "\n")
                    want := strings.TrimRight(string(tc.contents[key].html), "\n")
                    if strings.Compare(got, want) != 0 {
                        t.Fatalf("unexpected html, got=%s, want=%s", got, want)
                    }
                }

                for i, meta := range postsMetadata {
                    if meta["title"] != tc.postsMetadata[i]["title"] {
                        t.Fatalf("unexpected post meta, got=%v, want=%v",
                            meta["title"], tc.postsMetadata[i]["title"])
                    }
                }
            }
        })   
    }
}

func Test_parseTemplates(t *testing.T) {
    tests := []struct{
        name            string
        dir             string
        contents        map[string]content
        postsMetadata   []map[string]interface{}
        templates       map[string]templ
        wantErr         bool
    }{
        {
            name: "happy path",
            dir: "../testdata/test_parse_templates_good",
            contents: map[string]content{
                "test": {
                    kind: "content",
                    meta: map[string]interface{}{
                        "title": "test",
                    },
                    html: template.HTML("<p>test.md content</p>"),
                },
                "test_post": {
                    kind: "post",
                    meta: map[string]interface{}{
                        "title": "test_post",
                    },
                    html: template.HTML("<p>test_post.md content</p>"),
                },
            },
            postsMetadata: []map[string]interface{}{
                {"title": "test_post"},
            },
            templates: map[string]templ{
                "test": {
                    kind: "content",
                    html: "<!DOCTYPE html><html><head><title>test</title></head><body><p>test_post</p><p>test.md content</p></body></html>",
                },
                "test_post": {
                    kind: "post",
                    html: "<!DOCTYPE html><html><head><title>test_post</title></head><body><p>test_post.md content</p></body></html>",
                },
            },
            wantErr: false,
        },
        {
            name: "not found file",
            dir: "../testdata/test_parse_templates_bad",
            contents: map[string]content{
                "testies": {
                    kind: "content",
                    meta: map[string]interface{}{
                        "title": "testies",
                    },
                    html: template.HTML("<p>testies.md content</p>"),
                },
            },
            postsMetadata: []map[string]interface{}{
                {"title": "testies"},
            },
            templates: nil,
            wantErr: true,
        },
        {
            name: "not found template",
            dir: "../testdata/test_parse_templates_bad",
            contents: map[string]content{
                "test": {
                    kind: "content",
                    meta: map[string]interface{}{
                        "title": "test",
                    },
                    html: template.HTML("<p>test.md content</p>"),
                },
            },
            postsMetadata: []map[string]interface{}{
                {"title": "test"},
            },
            templates: nil,
            wantErr: true,
        },
    }

    for _, tc := range tests {
        t.Run(tc.name, func(t *testing.T) {
            templates, err := parseTemplates(tc.dir, tc.contents, tc.postsMetadata)
            if err != nil && tc.wantErr == false {
                t.Fatalf("unexpected error: got=%v", err)
            }

            if templates != nil {
                for key, val := range templates {
                    if val.kind != tc.templates[key].kind {
                        t.Fatalf("unexpected kind, got=%s, want=%s",
                            val.kind, tc.templates[key].kind)
                    }

                    if strings.Compare(val.html, tc.templates[key].html) != 0 {
                        t.Fatalf("unexpected html, got=%s, want=%s",
                            val.html, tc.templates[key].html)
                    }
                }
            }
        })
    }
}
