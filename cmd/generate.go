/*
* SPDX-License-Identifier: GPL-3.0-only
* Copyright (C) 2024 Kevin Suñer <ksuner@pm.me>
 */

package cmd

import (
	"bytes"
	"flag"
	"fmt"
	"html/template"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/yuin/goldmark"
	meta "github.com/yuin/goldmark-meta"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
)

func init() {
    commander["generate"] = Command{
        Flags: flag.NewFlagSet("generate", flag.ExitOnError),
        Env: nil,
        Info: `Transforms the Markdown files found in the content directory
to HTML, and embeds the resulting output into a Golang
html/template.

It uses the files provided by the provided theme during
the html/template parsing and execution phase.`,
        Usage: `Usage:
    statico [command] [flags]
    statico generate -theme=example`,
        Run: generate,
    }
}

func generate(cmd Command) {
    err := checkValues(cmd.Env, "theme")
    if err != nil {
        logger.Println(err.Error())
        fmt.Printf("\n%s\n", cmd.Usage)
        return
    }

    contents, postsMetadata, err := parseMarkdown(*cmd.Env["content"])
    if err != nil {
        logger.Fatalf("parseMarkdown: %v", err)
    }

    templates, err := parseTemplates(
        fmt.Sprintf("%s/%s", *cmd.Env["themes"], *cmd.Env["theme"]), contents, postsMetadata)
    if err != nil {
        logger.Fatalf("parseTemplates: %v", err)
    }

    err = os.RemoveAll(*cmd.Env["public"])
    if err != nil {
        logger.Fatalf("os.RemoveAll: %v", err)
    }

    err = os.MkdirAll(*cmd.Env["public-posts"], os.ModePerm)
    if err != nil {
        logger.Fatalf("os.MkdirAll: %v", err)
    }

    err = createFiles(*cmd.Env["public"], templates)
    if err != nil {
        logger.Fatalf("createFiles: %v", err)
    }

    err = copyDirectories(
        fmt.Sprintf("%s/%s", *cmd.Env["themes"], *cmd.Env["theme"]), *cmd.Env["public"])
    if err != nil {
        logger.Fatalf("copyDirectories: %v", err)
    }
}

type content struct {
    kind string
    meta map[string]interface{}
    html template.HTML
}

func parseMarkdown(dir string) (map[string]content, []map[string]interface{}, error) {
    var paths []string
    walkFn := fs.WalkDirFunc(func(path string, d fs.DirEntry, err error) error {
        if !d.IsDir() {
            paths = append(paths, path)
        }

        return err
    })

    err := filepath.WalkDir(dir, walkFn)
    if err != nil {
        return nil, nil, err
    }

    contents := make(map[string]content, len(paths))
    postsMetadata := make([]map[string]interface{}, 0)

    for _, path := range paths {
        fileparts := strings.Split(path, "/")
        file := fileparts[len(fileparts)-1]
        filename := strings.Split(file, ".")[0]

        out, err := os.ReadFile(path)
        if err != nil {
            return nil, nil, err
        }

        html, ctx, err := convertMarkdownToHTML(out)
        if err != nil {
            return nil, nil, err
        }

        metadata, err := meta.TryGet(ctx)
        if err != nil {
            return nil, nil, err
        }

        if strings.Contains(path, "posts") {
            contents[filename] = content{kind: "post", meta: metadata, html: html}
            postsMetadata = append(postsMetadata, metadata)
            continue
        }

        contents[filename] = content{kind: "content", meta: meta.Get(ctx), html: html}
    }

    return contents, postsMetadata, nil
}

func convertMarkdownToHTML(input []byte) (template.HTML, parser.Context, error) {
    markdown := goldmark.New(
        goldmark.WithExtensions(
            extension.Table,
            extension.Strikethrough,
            extension.TaskList,
            meta.Meta,
        ),
    )

    ctx := parser.NewContext()
    var buf bytes.Buffer
    err := markdown.Convert(input, &buf, parser.WithContext(ctx))
    if err != nil {
        return template.HTML(""), nil, err
    }

    return template.HTML(buf.String()), ctx, nil
}

type templ struct {
    kind string
    html string
}

func parseTemplates(
    dir string,
    contents map[string]content,
    postsMetadata []map[string]interface{}) (map[string]templ, error) {
    paths := make([]string, 0)
    walkFn := fs.WalkDirFunc(func(path string, d fs.DirEntry, err error) error {
        if strings.Contains(path, "partials") && filepath.Ext(path) == ".tmpl" {
            paths = append(paths, path)
        }

        return err
    })

    err := filepath.WalkDir(dir, walkFn)
    if err != nil {
        return nil, err
    }

    var tplName string
    templates := make(map[string]templ, len(contents))

    for filename, content := range contents {
        if content.kind == "content" {
            tplName = filename
            paths = append(paths, fmt.Sprintf("%s/%s.html.tmpl", dir, filename)) }

        if content.kind == "post" {
            tplName = "post"
            paths = append(paths, fmt.Sprintf("%s/%s", dir, "post.html.tmpl")) }

        tpl, err := template.New(tplName).ParseFiles(paths...)
        if err != nil {
            return nil, err
        }

        var buf bytes.Buffer
        err = tpl.Execute(&buf, map[string]interface{}{
            "meta": content.meta, "html": content.html, "postsMetadata": postsMetadata})
        if err != nil {
            return nil, err
        }

        templates[filename] = templ{kind: content.kind, html: buf.String()}
    }

    return templates, nil
}

func createFiles(dir string, templates map[string]templ) error {
    for filename, templ := range templates {
        format := "%s/%s.html"
        if templ.kind == "post" { format = "%s/posts/%s.html" }

        file, err := os.Create(fmt.Sprintf(format, dir, filename))
        if err != nil {
            return err
        }

        _, err = file.WriteString(templ.html)
        if err != nil {
            return err
        }
    }

    return nil
}

func copyDirectories(src, dest string) error {
    walkFn := fs.WalkDirFunc(func(path string, d fs.DirEntry, err error) error {
        if d.Name() == "." { return nil }
        if filepath.Ext(d.Name()) == ".tmpl" { return nil }
        if d.Name() == ".git" || d.Name() == "partials" { return fs.SkipDir }

        if d.IsDir() {
            return os.Mkdir(fmt.Sprintf("%s/%s", dest, path), os.ModePerm)
        }

        out, err := os.ReadFile(fmt.Sprintf("%s/%s", src, path))
        if err != nil {
            return err
        }

        file, err := os.Create(fmt.Sprintf("%s/%s", dest, path))
        if err != nil {
            return err
        }
        defer file.Close()

        _, err = file.Write(out)
        return err
    })

    return fs.WalkDir(os.DirFS(src), ".", walkFn)
}

