/*
* SPDX-License-Identifier: GPL-3.0-only
* Copyright (C) 2024 Kevin Suñer <ksuner@pm.me>
 */

package cmd

import (
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"net/url"
	"os"
	"os/exec"
)

func init() {
    commander["init"] = Command{
        Flags: flag.NewFlagSet("init", flag.ExitOnError),
        Env: nil,
        Info: `Creates a new directory using the given project name,
with a set of sub-folders such as content, posts and themes,
which are required for the program to work.

The provided theme will be cloned inside the themes folder
using the <git> command.`,
        Usage: `Usage:
    statico [command] [flags]
    statico init -project=example -theme-url=https://github.com/user/example.git`,
        Run: initialize,
    }
}

func initialize(cmd Command) {
    err := checkValues(cmd.Env, "project", "theme-url")
    if err != nil {
        logger.Println(err.Error())
        fmt.Printf("\n%s\n", cmd.Usage)
        return
    }

    _, err = os.Stat(*cmd.Env["project"])
    if err != nil {
        if !errors.Is(err, fs.ErrNotExist) {
            logger.Fatalf("os.Stat: %v", err)
        }
    }

    err = os.MkdirAll(fmt.Sprintf("%s/%s", *cmd.Env["project"], *cmd.Env["content-posts"]), os.ModePerm)
    if err != nil {
        logger.Fatalf("os.MkdirAll: %v", err)
    }

    err = os.Mkdir(fmt.Sprintf("%s/%s", *cmd.Env["project"], *cmd.Env["themes"]), os.ModePerm)
    if err != nil {
        logger.Fatalf("os.Mkdir: %v", err)
    }

    _, err = url.ParseRequestURI(*cmd.Env["theme-url"])
    if err != nil {
        logger.Fatalf("url.ParseRequestURI: %v", err)
    }

    command := exec.Command("git", "clone", *cmd.Env["theme-url"])
    command.Dir = fmt.Sprintf("%s/%s", *cmd.Env["project"], *cmd.Env["themes"])
    if err := command.Run(); err != nil {
        logger.Fatalf("command.Run: %v", err)
    }

}

func checkValues(value map[string]*string, keys ...string) error {
    for _, key := range keys {
        if val, ok := value[key]; ok {
            if val == nil || len(*val) == 0 {
                return fmt.Errorf("missing -%s command-line flag", key)
            }
        }
    }

    return nil
}
