/*
* SPDX-License-Identifier: GPL-3.0-only
* Copyright (C) 2024 Kevin Suñer <ksuner@pm.me>
 */

package main

import (
	"fmt"
	"os"
	"statico/cmd"
)

var (
    contentDir      string = "content"
    publicDir       string = "public"
    themesDir       string = "themes"
    contentPostsDir string = fmt.Sprintf("%s/posts", contentDir)
    publicPostsDir  string = fmt.Sprintf("%s/posts", publicDir)
)

var env = map[string]*string{
    "content": &contentDir,
    "public": &publicDir,
    "themes": &themesDir,
    "content-posts": &contentPostsDir,
    "public-posts": &publicPostsDir,
}

func main() {
    commander := cmd.Start()
    
    if len(os.Args) < 2 {
        fmt.Printf("%s\n\n%s\n", commander["root"].Info, commander["root"].Usage)
        return
    }

    if cmd, ok := commander[os.Args[1]]; ok {
        cmd.Env = env
        cmd.Env["theme"] = cmd.Flags.String("theme", "", "theme name")
        cmd.Env["project"] = cmd.Flags.String("project", "", "project name")
        cmd.Env["theme-url"] = cmd.Flags.String("theme-url", "", "theme url")

        help := cmd.Flags.Bool("help", false, "useful information") 
        cmd.Flags.Parse(os.Args[2:])

        if *help {
            fmt.Printf("%s\n\n%s\n", cmd.Info, cmd.Usage)
            return
        }

        cmd.Run(cmd)
        return
    }
}

