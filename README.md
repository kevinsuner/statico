# statico

A minimalist static web page generator that lives in your terminal, built for individuals that prioritize content over features.